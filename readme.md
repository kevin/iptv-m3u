# IPTV M3U

Takes [iptv-org](https://github.com/iptv-org) data and creates a custom m3u

## Dependencies

* curl: https://curl.haxx.se/
* dos2unix: https://waterlan.home.xs4all.nl/dos2unix.html
* grep: https://www.gnu.org/software/grep/
* node/npm: https://nodejs.org/en/
* rows[cli]: http://turicas.info/rows/cli/
* sqlite3: https://www.sqlite.org

## Usage

1. Copy the example config files in the `config` folder to the equivalent file names without the `.example` (ie: copy `config/$x.example.json` to `config/$x.json`)
2. Edit the newly created non-example files in the `config` folder to customize the set of languages, countries and categories using the `id` attributes of the respective csv files here: https://github.com/iptv-org/database/blob/master/data
3. Run `npm install`
4. Run `./iptv-m3u-update`
5. Load your freshly generated `channels.m3u`
